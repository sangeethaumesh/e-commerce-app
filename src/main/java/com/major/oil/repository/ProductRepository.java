package com.major.oil.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.major.oil.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}

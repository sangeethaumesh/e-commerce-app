package com.major.oil.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.major.oil.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
